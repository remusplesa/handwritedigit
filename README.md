## Keras model deploy in a React app.

* Live @ https://remusplesa.gitlab.io/handwritedigit/ with gitlab pages CI
* Built wtih React and styled components
* Jupyter notebook used for data analysis and model training
* Responisve design with mobile version

![Application snapshot](appSnip.PNG)