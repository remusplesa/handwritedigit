import styled from "styled-components";

export const PlotArea = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  height: 500px;
  @media screen and (max-width: 1200px) {
    height: 300px;
  }
`;

export const BarPlotArea = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const BlankPlotBar = styled.div`
  width: 500px;
  height: 20px;
  margin: 0px 0px 0px 10px;
  background: #bfd8c6;
  border-radius: 20px;
  @media screen and (max-width: 1200px) {
    width: 280px;
    height: 15px;
  }
`;

export const ResultPlotBar = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: calc(${(props) => props.probability});
  min-width: 30px;
  height: 20px;
  margin: 0px;
  opacity: 0.7;
  background: #6fa87f;
  border-radius: 20px;
  @media screen and (max-width: 1200px) {
    height: 15px;
  }
`;

export const ResultPercentage = styled.a`
  font-size: 12px;
`;
