import styled from 'styled-components';

export const MainContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    background: white;

`;

export const Container = styled.div`
    display: flex;
    align-content: flex-start;
    flex-direction: column;
    width: 1200px;
    min-height: 100vh;
    @media screen and (max-width: 1200px){
        width: 320px;
    }
`;

export const AppArea = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    padding: 15px;
    background-color: white;
    box-shadow: 10px 10px 30px #E5E4E2;
    border-radius: 30px;
    margin-bottom:40px;
    @media screen and (max-width: 1200px){
        margin-bottom:15px;
        padding: 0px;
    }
`;

export const Title = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    height: 180px;
    width: 100%;
    margin: 0px;
    font-size: 80px;
    @media screen and (max-width: 1200px){
        font-size: 3rem;
        height: 120px;

    }
`;

export const Text = styled.div`
    font-size: 5rem;
    font-weight: bold;
    @media screen and (max-width: 1200px){
        font-size: 3rem;
    }
`;
