import styled from 'styled-components';

export const MyArticle = styled.article`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    padding:15px;
    margin-bottom: 20px;
`;

export const ArticleTitle = styled.h3`
    font-size: 2rem;
    margin: 0px; 
    padding-bottom: 5px;
    color: #303030;
`;

export const ArticleAuthor = styled.h3`
    font-size: 1rem;
    margin: 0px;
    padding-bottom: 5px;
    color: #303030;
`;
export const ArticleHr = styled.hr`
    border: 0;
    height: 2px;
    background: #E5E4E2;
    margin: 0px;
`;

export const ArticleContent = styled.p`
    margin: 10px;
    color: #303030;
`;