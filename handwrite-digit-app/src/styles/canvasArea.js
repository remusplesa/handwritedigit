import styled from 'styled-components';

export const CanvasArea = styled.section`
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 100%;
    height: 600px;
    @media screen and (max-width: 1200px){
        flex-direction: column;
        height: 650px;
    }
`;

export const CanvasPlace = styled.div`
    position: relative;
    height: 500px;
    width: 500px;
    @media screen and (max-width: 1200px){
        height: 280px;
        width: 280px;
    }
`;

export const ClearButton = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute; 
    top: 400px; 
    left: 40px; 
    height: 60px; 
    width: 60px; 
    background: #FE5F55; 
    border-radius:30px;
    opacity: 0.7;

    &:hover {
        opacity: 1;
        transform: scale(1.1);
        transition-duration: 0.3s;
    }

    @media screen and (max-width: 1200px){
        height: 45px; 
        width: 45px; 
        top: 230px; 
        left: 10px; 
    }
`;

export const PredictButton = styled(ClearButton)`
    left: 400px;
    background: #0DAB76;
    @media screen and (max-width: 1200px){
        top: 230px; 
        left: 230px; 
    }
`;

export const LoadingArea = styled.div`
    position: absolute;
    top: 200px;
    left: 120px;
    & > p {
        font-size: 30px;
        font-weight: bold;
        color: #808080
    }
    @media screen and (max-width: 1200px){
        top: 100px;
        left: 60px;
        & > p {
            font-size: 18px;
        }
    }
`