import React from "react";
import {
  MyArticle,
  ArticleTitle,
  ArticleAuthor,
  ArticleHr,
  ArticleContent,
} from "../styles/article";

export const MyArticleArea = (props) => {
  return (
    <MyArticle>
      <ArticleTitle>About the project</ArticleTitle>
      <ArticleAuthor>by Plesa Andrei</ArticleAuthor>
      <ArticleHr></ArticleHr>
      <ArticleContent>
        This project came to be after I started learning about React and CNN's.
      </ArticleContent>
      <ArticleContent>
        The first steps were to adapt the MNIST digit dataset to my usecase: A
        canvas where an user could draw a digit which we could recognize. The
        digits could be located anywhere in the canvas so generating randomly
        scaled images was the solution i chose. I have trained a simple Keras
        sequential CNN model with the resulted data. With the use of
        tensorflowjs_converter the model and weights were adapted to work with
        the js version of tensorflow.
      </ArticleContent>
      <ArticleContent>
        The React app loads the tf model when loading the canvas area and
        proceeds to pass the prediction state to the bar plot area after the
        user has called the predict functionality. The design was done using
        styled-components and flexbox.
      </ArticleContent>
      <ArticleContent>
        <a
          href="https://gitlab.com/remus.plesa/handwritedigit"
          target="_blank"
          rel="noopener noreferrer"
          style={{ textDecoration: "none" }}
        >
          Check it out on Gitlab{" "}
          <span role="img" aria-label="star">
            🌟
          </span>
        </a>
      </ArticleContent>
    </MyArticle>
  );
};
