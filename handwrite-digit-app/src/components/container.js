import React from 'react';
import {MyCanvas} from './canvasArea';
import {MyArticleArea} from './article';
import './style.css';
import {
    MainContainer,
    Container,
    AppArea,
    Title,
    Text,
} from '../styles/container';


const MyHeader = props => {
    return (
        <Title>
            <Text>{props.title}</Text>
        </Title>
    )
}


export class AppContainer extends React.Component{
    render(){
        return(
            <MainContainer>
                <Container>
                    <MyHeader title='HandwriteML'/>
                    <AppArea>
                        <MyCanvas></MyCanvas>
                        <MyArticleArea />
                    </AppArea>
                </Container>
            </MainContainer>
            
        )
    }
}