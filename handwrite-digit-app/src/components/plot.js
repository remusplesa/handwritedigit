import React from 'react';
import {
    PlotArea,
    BarPlotArea,
    BlankPlotBar,
    ResultPlotBar,
    ResultPercentage
}from '../styles/plot';


const PlotBar = props => {
    let probability = props.probability+'%';
    console.log(`probability ${props.item}: ${probability}`);
    return (
        <BarPlotArea> 
            <h3 style={{margin:0}}>{props.item}</h3>
            <BlankPlotBar >
                <ResultPlotBar probability={probability}>
                    <ResultPercentage>{probability}</ResultPercentage>
                </ResultPlotBar>
            </BlankPlotBar>
        </BarPlotArea>
    )
}

export const PlotContainer = props => {
    return(
        <PlotArea id='PlotArea'>
            {props.prediction.map((p, index)=>{
                return (
                    <PlotBar key={index} item={index} probability={Math.round(p*100) }/>
                )
            })}
        </PlotArea>
    )
}