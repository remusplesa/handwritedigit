import React from "react";
import { Canvas } from "./canvas";
import { PlotContainer } from "./plot";
import { Predict } from "./predict";
import { CanvasArea } from "../styles/canvasArea";

import * as tf from "@tensorflow/tfjs";

export class MyCanvas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prediction: [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1],
    };
    this.predict = this.predict.bind(this);
  }

  async predict() {
    let myCanvas = document.getElementById("toPredict");
    let ctx = myCanvas.getContext("2d");
    let imageToPredict = ctx.getImageData(
      0,
      0,
      myCanvas.clientWidth,
      myCanvas.clientWidth
    );

    let myPrediction = new Predict(this.model, imageToPredict);

    console.log("Predicting...");
    let result = await myPrediction.predict();
    result = Array.prototype.slice.call(result);
    this.setState({ prediction: result });
  }

  async componentDidMount() {
    this.model = await tf.loadLayersModel(
      "https://mymodelname.blob.core.windows.net/mlmodel/model.json"
    );
    this.model.summary();
    this.myCanvas = document.getElementById("toPredict");
  }
  
  render() {
    return (
      <CanvasArea id="CanvasArea">
        <Canvas predict={this.predict} />
        <PlotContainer prediction={this.state.prediction} />
      </CanvasArea>
    );
  }
}
