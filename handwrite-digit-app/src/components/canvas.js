import React, { useState, useEffect } from "react";
import { CanvasPlace, ClearButton, PredictButton, LoadingArea } from "../styles/canvasArea";

const Loading = (props) => {
  const [hidden, setHidden] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setHidden(true);
    }, 6000);
  });

  if (hidden) {
    return <span></span>;
  } else {
    return (
      <LoadingArea><p>Loading weights...</p></LoadingArea>  
    );
  }
};

export class Canvas extends React.Component {
  constructor(props) {
    super(props);
    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.endPaintEvent = this.endPaintEvent.bind(this);
    this.offsetTop = null;
    this.offsetLef = null;
  }

  isPainting = false;
  prevPos = { clientX: 0, clientY: 0 };
  userStrokeStyle = "#0e1111";

  onMouseDown = ({ nativeEvent }) => {
    const { clientX, clientY } = nativeEvent;
    this.isPainting = true;
    this.prevPos = { clientX, clientY };
    this.offsetTop = this.canvasBox.getBoundingClientRect().top;
    this.offsetLeft = this.canvasBox.getBoundingClientRect().left;
  };

  onMouseMove = ({ nativeEvent }) => {
    if (this.isPainting) {
      const { clientX, clientY } =
        nativeEvent.type === "mousemove" ? nativeEvent : nativeEvent.touches[0];
      const offSetData = { clientX, clientY };
      this.paint(this.prevPos, offSetData, this.userStrokeStyle);
    }
  };

  endPaintEvent = () => {
    if (this.isPainting) {
      this.isPainting = false;
    }
  };

  paint = (prevPos, currPos, strokeStyle) => {
    const { clientX, clientY } = currPos;
    const { clientX: x, clientY: y } = prevPos;
    this.ctx.beginPath();
    this.ctx.strokeStyle = strokeStyle;
    this.ctx.moveTo(x - this.offsetLeft, y - this.offsetTop);
    this.ctx.lineTo(clientX - this.offsetLeft, clientY - this.offsetTop);
    this.ctx.stroke();
    this.prevPos = { clientX, clientY };
  };

  clearCanvas = () => {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    console.log("Canvas cleared.");
  };

  componentDidMount = () => {
    // Here we set up the properties of the canvas element.
    this.canvas.addEventListener("onTouchStart", this.onMouseMove, {
      passive: false,
    });
    this.canvas.width = this.canvasBox.clientWidth;
    this.canvas.height = this.canvasBox.clientWidth;
    this.ctx = this.canvas.getContext("2d");
    this.ctx.lineJoin = "round";
    this.ctx.lineCap = "round";
    this.ctx.lineWidth = 35;
  };

  render() {
    return (
      <CanvasPlace ref={(ref) => (this.canvasBox = ref)}>
        <canvas
          id="toPredict"
          ref={(ref) => (this.canvas = ref)}
          style={{
            background: "f8f8f8",
            borderStyle: "solid",
            borderWidth: 2,
            borderColor: "#E5E4E2",
            touchAction: "none",
          }}
          onMouseDown={this.onMouseDown}
          onMouseLeave={this.endPaintEvent}
          onMouseUp={this.endPaintEvent}
          onMouseMove={this.onMouseMove}
          onTouchStart={this.onMouseDown}
          onTouchEnd={this.endPaintEvent}
          onTouchMove={this.onMouseMove}
        />
        <Loading></Loading>
        <ClearButton onClick={this.clearCanvas}>
          <ion-icon name="trash-bin-outline" size="large"></ion-icon>
        </ClearButton>
        <PredictButton onClick={this.props.predict}>
          <ion-icon name="help-outline" size="large"></ion-icon>
        </PredictButton>
      </CanvasPlace>
    );
  }
}
