import * as tf from '@tensorflow/tfjs';

export class Predict {
    constructor(Model, toPredict){
        this.model = Model;
        this.toPredict = toPredict;
    }

    preprocess(imgData){
        return tf.tidy(()=>{
            //convert the image data to a tensor 
            let tensor = tf.browser.fromPixels(imgData, 1) //numChannels =1
            //resize to 56 x 56 
            const resized = tf.image.resizeBilinear(tensor, [56, 56]).toFloat()
            // Normalize the image 
            const offset = tf.scalar(255.0);
            const normalized =resized.div(offset).ceil(); 
            //We add a dimension to get a batch shape 
            const batched = normalized.expandDims(0)

            return batched
        })
    }

    predict(){
        return this.model.predict(this.preprocess(this.toPredict)).dataSync();
    }
}