import React from 'react';
import {AppContainer} from './components/container';

function App() {
  return (
    <div>
      <AppContainer></AppContainer>
    </div>
  );
}

export default App;
